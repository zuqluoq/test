package com.example.myapplication.data.api.dto

data class CoordApiDto(
    val lon: Double,
    val lat: Double
)