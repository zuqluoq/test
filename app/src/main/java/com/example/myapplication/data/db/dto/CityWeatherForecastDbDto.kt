package com.example.myapplication.data.db.dto

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.myapplication.data.db.WeatherForecastsConverter

@Entity(tableName = "city_weather_forecast")
@TypeConverters(WeatherForecastsConverter::class)
data class CityWeatherForecastDbDto(
    @PrimaryKey @ColumnInfo(name = "city_id") val cityId: Int,
    @ColumnInfo(name = "city_name") val cityName: String,
    val forecast: List<WeatherForecastDbDto>
)