package com.example.myapplication

import com.example.myapplication.domain.model.TempUnit
import com.example.myapplication.utils.DateUtils
import org.junit.Assert.*
import org.junit.Test
import java.util.*


class UtilityTest {

    @Test
    fun `test format weather forecast date`() {
        val formattedDate = DateUtils.getStringByDateTime(
            1642065245,
            DateUtils.WEATHER_FORECAST_DATE_FORMAT,
            Locale.US,
            TimeZone.getTimeZone("Asia/Ho_Chi_Minh")
        )
        assertEquals("Thu, 13 Jan 2022", formattedDate)
    }

    @Test
    fun `test temperature unit matching locale`() {
        assertEquals(TempUnit.Fahrenheit, TempUnit.getTempUnitFromLocale(Locale.US))
    }
}