package com.example.myapplication.utils

import android.content.Context
import android.net.Network
import com.example.myapplication.R
import com.example.myapplication.data.api.NetworkFailure
import com.example.myapplication.data.base.Result

class ErrorHelper(val context: Context) {

    fun getMessageFromFailure(failure: Result.Failure): String {
        val errorId = when (failure) {
            is NetworkFailure ->
                when (failure) {
                    is NetworkFailure.NetworkError -> R.string.message_network_error
                    is NetworkFailure.SocketTimeout -> R.string.message_socket_timeout
                    is NetworkFailure.CityNotFound -> R.string.message_city_not_found
                    is NetworkFailure.ServerError -> R.string.message_server_error
                    is NetworkFailure.ClientError -> R.string.message_client_error
                    else -> R.string.message_something_went_wrong
                }
            else -> R.string.message_something_went_wrong

        }

        return context.getString(errorId)
    }
}