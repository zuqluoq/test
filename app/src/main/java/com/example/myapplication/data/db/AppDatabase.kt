package com.example.myapplication.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.myapplication.data.db.dao.CityWeatherForecastDao
import com.example.myapplication.data.db.dto.CityWeatherForecastDbDto

@Database(
    entities = [CityWeatherForecastDbDto::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase: RoomDatabase() {

    abstract fun cityWeatherForecastDao(): CityWeatherForecastDao
}