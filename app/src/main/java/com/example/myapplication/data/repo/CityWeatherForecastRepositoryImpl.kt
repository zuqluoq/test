package com.example.myapplication.data.repo

import com.example.myapplication.data.api.ApiService
import com.example.myapplication.data.api.dto.CityWeatherForecastApiDto
import com.example.myapplication.data.api.param.CityWeatherForecastParam
import com.example.myapplication.data.base.Result
import com.example.myapplication.data.db.dao.CityWeatherForecastDao
import com.example.myapplication.data.db.dto.CityWeatherForecastDbDto
import com.example.myapplication.data.db.dto.WeatherForecastDbDto
import com.example.myapplication.domain.model.CityWeatherForecastModel
import com.example.myapplication.domain.model.TempUnit
import com.example.myapplication.domain.model.WeatherForecastModel
import com.example.myapplication.domain.repo.CityWeatherForecastRepository

class CityWeatherForecastRepositoryImpl(
    private val apiService: ApiService,
    private val cityWeatherForecastDao: CityWeatherForecastDao
): CityWeatherForecastRepository() {

    private val tempUnit = TempUnit.getTempUnitFromLocale()

    override suspend fun getCityWeatherForecast(
        param: CityWeatherForecastParam
    ): Result<CityWeatherForecastModel> {
         return when (val apiResult = apiService.getCityWeatherForecast(param.toQuery())) {
            is Result.Success -> {
                var domainModel: CityWeatherForecastModel? = null
                apiResult.data?.let {
                    domainModel = networkToDomain(it)
                    cityWeatherForecastDao.insert(networkToLocal(it))
                }
                Result.Success(domainModel)
            }
            else -> apiResult as Result.Failure
        }
    }

    override suspend fun getCityWeatherForecastDb(cityName: String): CityWeatherForecastModel? =
        cityWeatherForecastDao.get(cityName)?.let { localToDomain(it) }

    override fun networkToDomain(networkDto: CityWeatherForecastApiDto) = CityWeatherForecastModel(
        cityId = networkDto.city.id,
        cityName = networkDto.city.name,
        forecast = networkDto.list.map {
            WeatherForecastModel(
                dateTime = it.dt,
                avgTemp = it.temp.getAvgTemp(),
                pressure = it.pressure.toInt(),
                humidity = it.humidity,
                description = it.weather.firstOrNull()?.description ?: "",
                tempUnit = tempUnit
            )
        }
    )

    override fun networkToLocal(networkDto: CityWeatherForecastApiDto) = CityWeatherForecastDbDto(
        cityId = networkDto.city.id,
        cityName = networkDto.city.name,
        forecast = networkDto.list.map {
            WeatherForecastDbDto(
                dateTime = it.dt,
                avgTemp = it.temp.getAvgTemp(),
                pressure = it.pressure,
                humidity = it.humidity,
                description = it.weather.firstOrNull()?.description ?: "",
                tempUnit = tempUnit.unit
            )
        }
    )

    override fun localToDomain(localDto: CityWeatherForecastDbDto) = CityWeatherForecastModel(
        cityId = localDto.cityId,
        cityName = localDto.cityName,
        forecast = localDto.forecast.map {
            WeatherForecastModel(
                dateTime = it.dateTime,
                avgTemp = it.avgTemp,
                pressure = it.pressure.toInt(),
                humidity = it.humidity,
                description = it.description,
                tempUnit = TempUnit.findTempUnitFromUnit(it.tempUnit)
            )
        }
    )
}