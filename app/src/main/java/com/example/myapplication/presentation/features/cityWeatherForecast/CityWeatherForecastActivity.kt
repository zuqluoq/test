package com.example.myapplication.presentation.features.cityWeatherForecast

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import androidx.lifecycle.ViewModel
import com.example.myapplication.databinding.ActivityCityWeatherForecastBinding
import com.example.myapplication.presentation.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class CityWeatherForecastActivity:
    BaseActivity<ActivityCityWeatherForecastBinding, ViewModel>() {

    override val bindingInflater: (LayoutInflater) -> ActivityCityWeatherForecastBinding =
        ActivityCityWeatherForecastBinding::inflate

    override val viewModel: ViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("onCreate")
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("onDestroy")
    }


    override fun onRestart() {
        super.onRestart()
        Timber.d("xxx onRestart")
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        Timber.d("xxx nConfigurationChanged")
        super.onConfigurationChanged(newConfig)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

}