package com.example.myapplication.domain.usecase

import com.example.myapplication.data.api.param.CityWeatherForecastParam
import com.example.myapplication.data.base.Result
import com.example.myapplication.data.base.UseCase
import com.example.myapplication.domain.model.CityWeatherForecastModel
import com.example.myapplication.domain.repo.CityWeatherForecastRepository
import javax.inject.Inject

class GetCityWeatherForecastUseCase @Inject constructor(
    private val cityWeatherForecastRepo: CityWeatherForecastRepository
): UseCase<CityWeatherForecastParam, Result<CityWeatherForecastModel>>() {

    override suspend fun run(param: CityWeatherForecastParam) =
        cityWeatherForecastRepo.getCityWeatherForecast(param)
}