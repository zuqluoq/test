package com.example.myapplication.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
enum class TempUnit(val unit: String, val label: String) : Parcelable {
    Kelvin("standard","°K"),
    Celsius("metric", "°C"),
    Fahrenheit("imperial", "°F");

    companion object {

        fun getTempUnitFromLocale(locale: Locale = Locale.getDefault()) = when (locale) {
            Locale.US -> Fahrenheit
            else -> Celsius
        }

        fun findTempUnitFromUnit(unit: String) =
            values().firstOrNull { it.unit == unit } ?: Celsius
    }
}