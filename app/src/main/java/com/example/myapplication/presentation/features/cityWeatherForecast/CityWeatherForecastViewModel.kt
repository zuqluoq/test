package com.example.myapplication.presentation.features.cityWeatherForecast

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.myapplication.BuildConfig
import com.example.myapplication.data.api.param.CityWeatherForecastParam
import com.example.myapplication.data.base.Result
import com.example.myapplication.domain.model.CityWeatherForecastModel
import com.example.myapplication.domain.model.TempUnit
import com.example.myapplication.domain.usecase.GetCityWeatherForecastDbUseCase
import com.example.myapplication.domain.usecase.GetCityWeatherForecastUseCase
import com.example.myapplication.presentation.base.BaseViewModel
import com.example.myapplication.utils.coSwitchMap
import dagger.hilt.android.lifecycle.HiltViewModel
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class CityWeatherForecastViewModel @Inject constructor(
    private val getCityWeatherForecastUseCase: GetCityWeatherForecastUseCase,
    private val getCityWeatherForecastDbUseCase: GetCityWeatherForecastDbUseCase
): BaseViewModel() {

    private var _cityWeatherForecastInvoker = MutableLiveData<CityWeatherForecastParam>()

    val cityWeatherForecastResult = _cityWeatherForecastInvoker.coSwitchMap(Result.Loading) {
        getCityWeatherForecastUseCase.invoke(viewModelScope, it)
    }

    val cityWeatherForecastDbResult = cityWeatherForecastResult.coSwitchMap {
        if (it is Result.Failure) {
            _cityWeatherForecastInvoker.value?.cityName?.let { cityName ->
                getCityWeatherForecastDbUseCase.invoke(viewModelScope, cityName)
            }
        } else null
    }

    fun getCityWeatherForecast(cityName: String) {
        Timber.d("getCityWeatherForecast - cityName=$cityName")
        _cityWeatherForecastInvoker.value = CityWeatherForecastParam(
            cityName, BuildConfig.API_KEY, 7, TempUnit.getTempUnitFromLocale().unit
        )
    }
}