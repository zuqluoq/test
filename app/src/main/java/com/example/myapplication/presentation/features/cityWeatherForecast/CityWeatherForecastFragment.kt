package com.example.myapplication.presentation.features.cityWeatherForecast

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.base.Result
import com.example.myapplication.databinding.FragmentCityWeatherForecastBinding
import com.example.myapplication.presentation.base.BaseFragment
import com.example.myapplication.utils.Constants
import com.example.myapplication.utils.ErrorHelper
import com.example.myapplication.utils.observe
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class CityWeatherForecastFragment:
    BaseFragment<FragmentCityWeatherForecastBinding, CityWeatherForecastViewModel>() {

    override val viewModel: CityWeatherForecastViewModel by viewModels()

    override val bindingInflater: (LayoutInflater) -> FragmentCityWeatherForecastBinding =
        FragmentCityWeatherForecastBinding::inflate

    private lateinit var weatherForecastAdapter: WeatherForecastAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Timber.d("onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("onCreate")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("onViewCreated")

        initViews()
        initViewEvents()
        initDataListeners()
    }

    private fun initViews() = binding?.run {
        weatherForecastAdapter = WeatherForecastAdapter()
        val divider = DividerItemDecoration(requireContext(), RecyclerView.VERTICAL)
        rvWeatherForecast.addItemDecoration(divider)
        rvWeatherForecast.layoutManager = LinearLayoutManager(requireContext())
        rvWeatherForecast.adapter = weatherForecastAdapter
    }

    private fun initViewEvents() = binding?.run {
        btnGetWeather.setOnClickListener {
            val cityName = etCityName.text.toString()
            if (cityName.length < Constants.CITY_NAME_MIN_LENGTH) {
                Toast.makeText(
                    requireContext(),
                    R.string.toast_city_name_minimum_length_required,
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
            viewModel.getCityWeatherForecast(cityName)
            weatherForecastAdapter.setWeatherForecasts(listOf())
        }
    }

    private fun initDataListeners() = binding?.run {
        observe(viewModel.cityWeatherForecastResult) { result ->
            when (result) {
                is Result.Success -> {
                    result.data?.let {
                        weatherForecastAdapter.setWeatherForecasts(it.forecast)
                    }
                }
                is Result.Failure -> {
                    Toast.makeText(
                        requireContext(),
                        ErrorHelper(requireContext()).getMessageFromFailure(result),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            val isLoading = result is Result.Loading
            pbLoading.visibility = if (isLoading) View.VISIBLE else View.GONE
            btnGetWeather.isEnabled = !isLoading
            etCityName.isEnabled = !isLoading
        }

        observe(viewModel.cityWeatherForecastDbResult) {
            it?.let {
                weatherForecastAdapter.setWeatherForecasts(it.forecast)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        Timber.d("onDetach")
    }
}