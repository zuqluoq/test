package com.example.myapplication.data.api

import com.example.myapplication.data.base.Result
import java.io.IOException
import java.net.SocketTimeoutException

sealed class NetworkFailure: Result.Failure() {
    object ClientError: NetworkFailure()
    object CityNotFound: NetworkFailure()
    object ServerError: NetworkFailure()
    object NetworkError: NetworkFailure()
    object SocketTimeout: NetworkFailure()
    object UnknownError: NetworkFailure()

    class NetworkFailureDispatcherImpl: NetworkFailureDispatcher {
        override fun failureFromCode(code: Int, body: Any?): Failure {
            return when (code) {
                404 -> CityNotFound
                in 400..499 -> ClientError
                in 500..599 -> ServerError
                else -> UnknownError
            }
        }

        override fun failureFromThrowable(throwable: Throwable): Failure =
            when (throwable) {
                is IOException -> NetworkError
                is SocketTimeoutException -> SocketTimeout
                else -> UnknownError
            }

    }

}