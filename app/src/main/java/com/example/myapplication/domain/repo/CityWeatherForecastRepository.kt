package com.example.myapplication.domain.repo

import com.example.myapplication.data.api.dto.CityWeatherForecastApiDto
import com.example.myapplication.data.api.param.CityWeatherForecastParam
import com.example.myapplication.data.base.BaseMappingRepository
import com.example.myapplication.data.base.Result
import com.example.myapplication.data.db.dto.CityWeatherForecastDbDto
import com.example.myapplication.domain.model.CityWeatherForecastModel

abstract class CityWeatherForecastRepository :
    BaseMappingRepository<CityWeatherForecastApiDto, CityWeatherForecastDbDto, CityWeatherForecastModel>() {

    abstract suspend fun getCityWeatherForecast(
        param: CityWeatherForecastParam
    ): Result<CityWeatherForecastModel>

    abstract suspend fun getCityWeatherForecastDb(
        cityName: String
    ): CityWeatherForecastModel?
}