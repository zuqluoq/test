package com.example.myapplication.data.api

import com.example.myapplication.data.base.Result

interface NetworkFailureDispatcher {
    fun failureFromCode(code: Int, body: Any?): Result.Failure

    fun failureFromThrowable(throwable: Throwable): Result.Failure
}