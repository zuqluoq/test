package com.example.myapplication.data.api.dto

data class CityApiDto(
    val id: Int,
    val name: String,
    val coord: CoordApiDto,
    val country: String,
    val population: Int,
    val timezone: Int
)
