package com.example.myapplication.utils

import com.example.myapplication.domain.model.WeatherForecastModel
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    const val WEATHER_FORECAST_DATE_FORMAT = "EEE, d MMM yyyy"

    fun getStringByDate(
        date: Date,
        format: String = WEATHER_FORECAST_DATE_FORMAT,
        locale: Locale,
        timeZone: TimeZone,
    ): String {
        val sdf = SimpleDateFormat(format, locale)
        sdf.timeZone = timeZone
        return sdf.format(date)
    }

    fun getStringByDateTime(
        dateTime: Int,
        format: String,
        locale: Locale = Locale.getDefault(),
        timeZone: TimeZone = TimeZone.getDefault()
    ): String {
        return getStringByDate(Date(dateTime * 1000L), format, locale, timeZone)
    }

    fun formatWeatherForecastDate(weatherForecast: WeatherForecastModel) =
        getStringByDateTime(weatherForecast.dateTime, WEATHER_FORECAST_DATE_FORMAT)
}