package com.example.myapplication.usecase

import com.example.myapplication.data.api.param.CityWeatherForecastParam
import com.example.myapplication.data.base.Result
import com.example.myapplication.domain.repo.CityWeatherForecastRepository
import com.example.myapplication.domain.usecase.GetCityWeatherForecastDbUseCase
import com.example.myapplication.domain.usecase.GetCityWeatherForecastUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class GetCityWeatherForecastDbUseCaseTest {

    private lateinit var getCityWeatherForecastDbUseCase: GetCityWeatherForecastDbUseCase
    @MockK
    private lateinit var cityWeatherForecastRepo: CityWeatherForecastRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        getCityWeatherForecastDbUseCase = GetCityWeatherForecastDbUseCase(cityWeatherForecastRepo)
        coEvery { cityWeatherForecastRepo.getCityWeatherForecastDb("") } returns null
    }

    @Test
    fun `should get data from repository`() {
        runBlocking { getCityWeatherForecastDbUseCase.invoke(GlobalScope, "") }
        coVerify(exactly = 1) { cityWeatherForecastRepo.getCityWeatherForecastDb("") }
    }
}