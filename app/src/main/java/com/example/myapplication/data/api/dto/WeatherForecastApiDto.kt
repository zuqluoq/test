package com.example.myapplication.data.api.dto

data class WeatherForecastApiDto(
    val dt: Int,
    val pressure: Double,
    val humidity: Int,
    val weather: List<WeatherApiDto>,
    val temp: TempApiDto
)
