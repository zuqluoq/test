package com.example.myapplication.usecase

import com.example.myapplication.data.api.param.CityWeatherForecastParam
import com.example.myapplication.data.base.Result
import com.example.myapplication.domain.repo.CityWeatherForecastRepository
import com.example.myapplication.domain.usecase.GetCityWeatherForecastUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class GetCityWeatherForecastUseCaseTest {

    private lateinit var getCityWeatherForecastUseCase: GetCityWeatherForecastUseCase
    @MockK
    private lateinit var cityWeatherForecastRepo: CityWeatherForecastRepository
    @MockK
    private lateinit var cityWeatherForecastParam: CityWeatherForecastParam

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        getCityWeatherForecastUseCase = GetCityWeatherForecastUseCase(cityWeatherForecastRepo)
        coEvery { cityWeatherForecastRepo.getCityWeatherForecast(cityWeatherForecastParam) } returns Result.Success(null)
    }

    @Test
    fun `should get data from repository`() {
        runBlocking { getCityWeatherForecastUseCase.invoke(GlobalScope, cityWeatherForecastParam) }
        coVerify(exactly = 1) { cityWeatherForecastRepo.getCityWeatherForecast(cityWeatherForecastParam) }
    }
}