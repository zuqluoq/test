package com.example.myapplication.data.db.dto

data class WeatherForecastDbDto(
    val dateTime: Int,
    val avgTemp: Int,
    val pressure: Double,
    val humidity: Int,
    val description: String,
    val tempUnit: String
)