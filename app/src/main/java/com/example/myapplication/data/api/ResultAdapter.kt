package com.example.myapplication.data.api

import com.example.myapplication.data.base.Result
import okhttp3.Request
import okio.Timeout
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit

class ResultAdapter<T>(
    private val responseType: Type,
    private val failureDispatcher: NetworkFailureDispatcher
): CallAdapter<T, Call<Result<T>>> {

    override fun responseType(): Type = responseType

    override fun adapt(call: Call<T>): Call<Result<T>> = ResultCall(call, failureDispatcher)

}