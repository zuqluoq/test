package com.example.myapplication.data.api.param

data class CityWeatherForecastParam(
    val cityName: String,
    val appId: String,
    val forecastLength: Int,
    val tempUnit: String
): QueryParam() {

    override fun toQuery(): Map<String, Any> {
        return mapOf<String, Any>(
            "q" to cityName,
            "appid" to appId,
            "cnt" to forecastLength,
            "units" to tempUnit
        )
    }
}