package com.example.myapplication.data.api.dto

data class CityWeatherForecastApiDto(
    val city: CityApiDto,
    val list: List<WeatherForecastApiDto>
)
