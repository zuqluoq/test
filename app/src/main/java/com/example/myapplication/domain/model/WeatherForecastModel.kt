package com.example.myapplication.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class WeatherForecastModel(
    val dateTime: Int,
    val avgTemp: Int,
    val pressure: Int,
    val humidity: Int,
    val description: String,
    val tempUnit: TempUnit
): Parcelable