package com.example.myapplication.data.api

import com.example.myapplication.data.api.dto.CityWeatherForecastApiDto
import com.example.myapplication.data.base.Result
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ApiService {

    @GET("/data/2.5/forecast/daily")
    suspend fun getCityWeatherForecast(
        @QueryMap param: @JvmSuppressWildcards Map<String, Any>
    ): Result<CityWeatherForecastApiDto>

}