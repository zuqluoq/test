package com.example.myapplication.data.base

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

abstract class UseCase<in Param, out Type> where Type : Any {
    abstract suspend fun run(param: Param): Type?

    suspend fun invoke(scope: CoroutineScope, param: Param): Type? =
        withContext(scope.coroutineContext + Dispatchers.IO) { run(param) }
}