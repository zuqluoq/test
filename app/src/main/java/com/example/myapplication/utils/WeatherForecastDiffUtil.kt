package com.example.myapplication.utils

import androidx.recyclerview.widget.DiffUtil
import com.example.myapplication.domain.model.WeatherForecastModel

class WeatherForecastDiffUtil(
    private val oldWeatherForecastList: List<WeatherForecastModel>,
    private val newWeatherForecastList: List<WeatherForecastModel>
): DiffUtil.Callback() {

    override fun getOldListSize() = oldWeatherForecastList.size

    override fun getNewListSize() = newWeatherForecastList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldWeatherForecastList[oldItemPosition] == newWeatherForecastList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldWeatherForecastList[oldItemPosition] == newWeatherForecastList[newItemPosition]
}