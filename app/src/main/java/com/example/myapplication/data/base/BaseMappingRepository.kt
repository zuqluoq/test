package com.example.myapplication.data.base

abstract class BaseMappingRepository<NetworkDto, LocalDto, DomainModel> {
    protected open fun networkToLocal(networkDto: NetworkDto): LocalDto? = null

    protected open fun localToDomain(localDto: LocalDto): DomainModel? = null

    protected open fun domainToLocal(domain: DomainModel): LocalDto? = null

    protected open fun networkToDomain(networkDto: NetworkDto): DomainModel? = null

    protected fun networksToLocals(list: List<NetworkDto>) =
        list.map {
            networkToLocal(it)
                ?: throw NotImplementedError("networkToLocal(NetworkDto) has not implemented yet!")
        }

    protected fun localsToDomains(list: List<LocalDto>) =
        list.map {
            localToDomain(it)
                ?: throw NotImplementedError("localToDomain(LocalDto) has not implemented yet!")
        }

    protected fun domainsToLocals(list: List<DomainModel>) =
        list.map {
            domainToLocal(it)
                ?: throw NotImplementedError("domainToLocal(DomainModel) has not implemented yet!")
        }

    protected fun networksToDomains(list: List<NetworkDto>) =
        list.map {
            networkToDomain(it)
                ?: throw NotImplementedError("networkToDomain(NetworkDto) has not implemented yet!")
        }
}