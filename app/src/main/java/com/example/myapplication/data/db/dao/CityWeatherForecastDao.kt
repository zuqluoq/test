package com.example.myapplication.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.myapplication.data.base.BaseDao
import com.example.myapplication.data.db.dto.CityWeatherForecastDbDto

@Dao
interface CityWeatherForecastDao: BaseDao<CityWeatherForecastDbDto> {

    @Query("SELECT * FROM city_weather_forecast WHERE city_name LIKE :name LIMIT 1")
    suspend fun get(name: String): CityWeatherForecastDbDto?
}