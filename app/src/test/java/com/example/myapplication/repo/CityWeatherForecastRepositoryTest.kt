package com.example.myapplication.repo

import com.example.myapplication.data.api.ApiService
import com.example.myapplication.data.api.param.CityWeatherForecastParam
import com.example.myapplication.data.base.Result
import com.example.myapplication.data.db.dao.CityWeatherForecastDao
import com.example.myapplication.data.repo.CityWeatherForecastRepositoryImpl
import com.example.myapplication.domain.model.CityWeatherForecastModel
import com.example.myapplication.domain.repo.CityWeatherForecastRepository
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import retrofit2.Call
import retrofit2.Response

class CityWeatherForecastRepositoryTest {
    private lateinit var cityWeatherForecastRepo: CityWeatherForecastRepositoryImpl
    @MockK
    private lateinit var apiService: ApiService
    @MockK
    private lateinit var cityWeatherForecastDao: CityWeatherForecastDao
    @MockK
    private lateinit var cityWeatherForecastParam: CityWeatherForecastParam

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        cityWeatherForecastRepo = CityWeatherForecastRepositoryImpl(apiService, cityWeatherForecastDao)
    }

    @Test
    fun `should get city weather forecast from web service`() {
        coEvery { apiService.getCityWeatherForecast(cityWeatherForecastParam.toQuery()) } returns Result.Success(null)
        runBlocking { cityWeatherForecastRepo.getCityWeatherForecast(cityWeatherForecastParam) }
        coVerify(exactly = 1) { apiService.getCityWeatherForecast(cityWeatherForecastParam.toQuery()) }
    }

    @Test
    fun `should get local city weather forecast from database`() {
        coEvery { cityWeatherForecastDao.get("") } returns null
        runBlocking { cityWeatherForecastRepo.getCityWeatherForecastDb("") }
        coVerify(exactly = 1) { cityWeatherForecastDao.get("") }
    }

}