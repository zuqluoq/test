package com.example.myapplication.domain.usecase

import com.example.myapplication.data.base.UseCase
import com.example.myapplication.domain.model.CityWeatherForecastModel
import com.example.myapplication.domain.repo.CityWeatherForecastRepository
import javax.inject.Inject

class GetCityWeatherForecastDbUseCase @Inject constructor(
    private val cityWeatherForecastRepo: CityWeatherForecastRepository
): UseCase<String, CityWeatherForecastModel>() {

    override suspend fun run(param: String): CityWeatherForecastModel? =
        cityWeatherForecastRepo.getCityWeatherForecastDb(param)

}