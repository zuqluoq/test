package com.example.myapplication.data.api.param

abstract class QueryParam {
    abstract fun toQuery(): Map<String, Any>
}