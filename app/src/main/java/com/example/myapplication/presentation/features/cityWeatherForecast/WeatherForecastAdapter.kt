package com.example.myapplication.presentation.features.cityWeatherForecast

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.ItemCityWeatherForecastBinding
import com.example.myapplication.domain.model.CityWeatherForecastModel
import com.example.myapplication.domain.model.WeatherForecastModel
import com.example.myapplication.utils.WeatherForecastDiffUtil

class WeatherForecastAdapter(
    private var weatherForecasts: List<WeatherForecastModel> = listOf()
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun setWeatherForecasts(newWeatherForecasts: List<WeatherForecastModel>) {
        val weatherForecastDiffUtil = WeatherForecastDiffUtil(weatherForecasts, newWeatherForecasts)
        val diffResult = DiffUtil.calculateDiff(weatherForecastDiffUtil)
        weatherForecasts = newWeatherForecasts
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return WeatherForecastViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? WeatherForecastViewHolder)?.bind(weatherForecasts[position])
    }

    override fun getItemCount(): Int = weatherForecasts.size

    inner class WeatherForecastViewHolder(
        parent: ViewGroup,
        private val binding: ItemCityWeatherForecastBinding =
            ItemCityWeatherForecastBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
    ): RecyclerView.ViewHolder(binding.root) {

        fun bind(data: WeatherForecastModel) {
            binding.weatherForecast = data
            binding.executePendingBindings()
        }
    }
}