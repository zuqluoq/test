package com.example.myapplication.data.api

import com.example.myapplication.data.base.Result
import okhttp3.Request
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.TimeUnit

class ResultCall<T>(
    private val proxy: Call<T>,
    private val failureDispatcher: NetworkFailureDispatcher
): Call<Result<T>> {

    override fun clone(): Call<Result<T>> = ResultCall(proxy.clone(), failureDispatcher)

    override fun execute(): Response<Result<T>> = throw NotImplementedError()

    override fun enqueue(callback: Callback<Result<T>>) = proxy.enqueue(object: Callback<T> {
        override fun onResponse(call: Call<T>, response: Response<T>) {
            val code = response.code()
            val result = if (code in 200 until 300) {
                Result.Success(response.body())
            } else {
                failureDispatcher.failureFromCode(code, response.errorBody()?.string())
            }

            callback.onResponse(this@ResultCall, Response.success(result))
        }

        override fun onFailure(call: Call<T>, t: Throwable) {
            val result = failureDispatcher.failureFromThrowable(t)
            callback.onResponse(this@ResultCall, Response.success(result))
        }
    })

    override fun isExecuted(): Boolean = proxy.isExecuted
    override fun cancel() = proxy.cancel()
    override fun isCanceled(): Boolean = proxy.isCanceled
    override fun request(): Request = proxy.request()
    override fun timeout(): Timeout = Timeout().timeout(15L, TimeUnit.SECONDS)
}

