package com.example.myapplication.data.api.dto

data class TempApiDto(
    val day: Double,
    val min: Double,
    val max: Double,
    val night: Double,
    val eve: Double,
    val morn: Double
) {

    fun getAvgTemp() = listOf(day, night, eve, morn).average().toInt()
}