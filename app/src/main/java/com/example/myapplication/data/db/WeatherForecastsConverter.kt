package com.example.myapplication.data.db

import androidx.room.TypeConverter
import com.example.myapplication.data.db.dto.WeatherForecastDbDto
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import timber.log.Timber

class WeatherForecastsConverter {

    @TypeConverter
    fun fromString(data: String?): List<WeatherForecastDbDto> =
        data?.let {
            val type = object: TypeToken<List<WeatherForecastDbDto>>() {}.type
            try {
               Gson().fromJson(it, type)
            } catch (e: Exception) {
                Timber.e("fromString - e=${e.message}")
                e.printStackTrace()
                emptyList()
            }
        } ?: emptyList()

    @TypeConverter
    fun toString(forecast: List<WeatherForecastDbDto>): String? =
        if (forecast.isEmpty()) {
            null
        } else {
            try {
                Gson().toJson(forecast)
            } catch (e: Exception) {
                Timber.e("toString - e=${e.message}")
                e.printStackTrace()
                null
            }
        }

}