package com.example.myapplication.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap

fun <X, Y> LiveData<X>.coSwitchMap(
    initialValue: Y? = null,
    transform: suspend (X) -> Y
): LiveData<Y> = this.switchMap {
    liveData {
        if (initialValue != null) emit(initialValue)
        emit(transform(it))
    }
}