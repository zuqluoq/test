package com.example.myapplication.di

import android.content.Context
import com.example.myapplication.BuildConfig
import com.example.myapplication.data.api.ApiService
import com.example.myapplication.data.api.NetworkFailure
import com.example.myapplication.data.api.NetworkFailureDispatcher
import com.example.myapplication.data.api.ResultCallAdapterFactory
import com.example.myapplication.utils.hasNetwork
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideHttpClient(
        @ApplicationContext context: Context
    ): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(15L, TimeUnit.SECONDS)
            .readTimeout(15L, TimeUnit.SECONDS)
            .cache(Cache(context.cacheDir,  5 * 1024 * 1024L))
            .addInterceptor(
                HttpLoggingInterceptor().setLevel(
                    if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                    else HttpLoggingInterceptor.Level.NONE
                )
            )
            .addInterceptor { chain ->
                var request = chain.request()
                if (context.hasNetwork()) {
                    request = request.newBuilder()
                        .header("Cache-Control", "public, max-age=" + 60).build()
                }
                chain.proceed(request)
            }
            .build()


    @Singleton
    @Provides
    fun provideNetworkFailureDispatcher(): NetworkFailureDispatcher =
        NetworkFailure.NetworkFailureDispatcherImpl()

    @Singleton
    @Provides
    fun provideWebService(
        okHttpClient: OkHttpClient,
        failureDispatcher: NetworkFailureDispatcher
    ) : ApiService =
        Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(ResultCallAdapterFactory(failureDispatcher))
            .build()
            .create(ApiService::class.java)
}
