package com.example.myapplication.di

import com.example.myapplication.data.api.ApiService
import com.example.myapplication.data.db.dao.CityWeatherForecastDao
import com.example.myapplication.data.repo.CityWeatherForecastRepositoryImpl
import com.example.myapplication.domain.repo.CityWeatherForecastRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Singleton
    @Provides
    fun provideCityWeatherForecastRepo(
        apiService: ApiService,
        cityWeatherForecastDao: CityWeatherForecastDao
    ): CityWeatherForecastRepository =
        CityWeatherForecastRepositoryImpl(apiService, cityWeatherForecastDao)
}