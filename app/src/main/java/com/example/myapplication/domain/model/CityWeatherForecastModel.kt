package com.example.myapplication.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CityWeatherForecastModel(
    val cityId: Int,
    val cityName: String,
    val forecast: List<WeatherForecastModel>
): Parcelable