package com.example.myapplication.data.api.dto

data class WeatherApiDto(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
)
