package com.example.myapplication.viewmodel

import android.os.Build
import com.example.myapplication.data.api.param.CityWeatherForecastParam
import com.example.myapplication.data.base.Result
import com.example.myapplication.domain.model.CityWeatherForecastModel
import com.example.myapplication.domain.usecase.GetCityWeatherForecastDbUseCase
import com.example.myapplication.domain.usecase.GetCityWeatherForecastUseCase
import com.example.myapplication.presentation.features.cityWeatherForecast.CityWeatherForecastViewModel
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class CityWeatherForecastViewModelTest {

    private lateinit var cityWeatherForecastViewModel: CityWeatherForecastViewModel
    @MockK
    private lateinit var getCityWeatherForecastUseCase: GetCityWeatherForecastUseCase
    @MockK
    private lateinit var getCityWeatherForecastDbUseCase: GetCityWeatherForecastDbUseCase
    @MockK
    private lateinit var cityWeatherForecastParam: CityWeatherForecastParam
    @MockK
    private lateinit var localCityWeatherForecast: CityWeatherForecastModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        cityWeatherForecastViewModel = CityWeatherForecastViewModel(getCityWeatherForecastUseCase, getCityWeatherForecastDbUseCase)
    }

    @Test
    fun `fetch city weather forecast should update live data`() {
        coEvery { getCityWeatherForecastUseCase.invoke(GlobalScope, cityWeatherForecastParam) } returns Result.Success(null)
        cityWeatherForecastViewModel.cityWeatherForecastResult.observeForever {
            assertTrue(it is Result.Success)
        }
        cityWeatherForecastViewModel.getCityWeatherForecast("")
    }

    @Test
    fun `load local city weather forecast when fetch city weather failed`() {
        coEvery { getCityWeatherForecastUseCase.invoke(GlobalScope, cityWeatherForecastParam) } returns Result.Failure()
        coEvery { getCityWeatherForecastDbUseCase.invoke(GlobalScope, "") } returns localCityWeatherForecast
        cityWeatherForecastViewModel.cityWeatherForecastResult.observeForever {
            assertTrue(it is Result.Failure)
        }
        cityWeatherForecastViewModel.cityWeatherForecastDbResult.observeForever {
            assertTrue(it is CityWeatherForecastModel)
        }
        cityWeatherForecastViewModel.getCityWeatherForecast("")
    }

    @After fun stop() {
        cityWeatherForecastViewModel.cityWeatherForecastResult.removeObserver{ }
    }
}